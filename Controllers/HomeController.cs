﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SampleTableServerSidePaging.Models;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace SampleTableServerSidePaging.Controllers
{
    public class PagingMode
    {
        public int Total { get; set; }
        public int Start { get; set; }
        public int Length { get; set; }
        public List<DataModel> Data { get; set; }
    }

    public class DataModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }

    public class JQueryDataTable
    {
        public int draw { get; set; }
        public int start { get; set; }
        public int length { get; set; }
        public string filter { get; set; }
    }
    public class JQueryResult<T>
    {
        public int draw { get; set; }

        public int start { get; set; }
        public int length { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }

        public IEnumerable<T> data { get; set; }

        public string error { get; set; }

        public string PartialView { get; set; }
    }
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly List<DataModel> _all;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
            _all = getData();
        }

        private List<DataModel> getData()
        {
            var result = new List<DataModel>();
            for (int i = 0; i <= 100; i++)
            {
                result.Add(new DataModel() { ID = i + 1, Name = $"Name_{i + 1}" });
            }
            return result;
        }
        [HttpGet]
        public IActionResult Index(int start = 0,int length = 10)
        {
            var param = new PagingMode()
            {
                Length = length,
                Start = start,
                Total = _all.Count,
                Data = _all.Skip(start).Take(length).ToList()
            };
            return View(param);
        }

        public IActionResult Privacy() => View();

        [HttpPost]
        public IActionResult Privacy([FromBody] JQueryDataTable param)
        {
            string filter = param.filter;
            //TODO: some filter
            List<DataModel> filtered = _all;
            if (!string.IsNullOrEmpty(filter))
            {
                filtered = _all.Where(p => p.ID % 2 == 0).ToList();
            }
            return Json(new JQueryResult<DataModel>
            {
                draw = param.draw,
                start = param.start,
                length = param.length,
                recordsTotal = filtered.Count,
                recordsFiltered = filtered.Count,
                data = filtered.Skip(param.start).Take(param.length).ToList()
            });
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error() => View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
    }
}
